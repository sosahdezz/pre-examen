/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Ejemplo2;

/**
 *
 * @author ChecheBossxKun
 */
public class Recibo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        MetodosRecibo2 recibo = new MetodosRecibo2();
       recibo.setNumContrato(103); 
       recibo.setFechaPago("23 Marzo 2019");
       recibo.setNombreEmpleado("Maria Acosta");
       recibo.setDomicilio("Av del Sol 1200");
       recibo.setTipoContrato(2);
       recibo.setNivelEstudios(2);
       recibo.setPagoBase(700.0f);
       recibo.setDiasTrabajados(15);   
       System.out.println("     El Calculo de el Subtotal es: " + recibo.calculoSubtotal());
       System.out.println("     El Calculo de el Impuesto es: " + recibo.calculoImpuesto());
       
       //OBJETO CONSTRUIDO POR ARGUMENTOS
       MetodosRecibo2 reci = new MetodosRecibo2 (103,"23 Marzo 2019","Maria Acosta","Av del Sol 1200",2,2,700.0f, 15);
       System.out.println ("Numero de Contrato:     " + reci.getNumContrato());
        System.out.println("Fecha de Contratacion:  " + reci.getFechaPago());
       System.out.println("Nombre Empleado:        " + reci.getNombreEmpleado());
       System.out.println("Domicilio:              " + reci.getDomicilio());
       System.out.println("Tipo de Contrato:       " + reci.getTipoContrato());
       System.out.println("Nivel de Estudios:      " + reci.getNivelEstudios());
       System.out.println("Pago Diario Base:       " + reci.getPagoBase());
       System.out.println("Dias Trabajados:        " + reci.getDiasTrabajados());
       
       
       //GENERA OBJETO CONSTRUIDO POR COPIA
       MetodosRecibo2 reci2 = new MetodosRecibo2(recibo);
       System.out.println("     El SubTotal es: " + reci2.calculoSubtotal());
       System.out.println("     El Impuesto es: " + reci2.calculoImpuesto());
       System.out.println("     El Total a Pagar es:" + reci2.calculoTotal());
    }
}
    