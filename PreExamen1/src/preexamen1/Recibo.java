/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package preexamen1;

/**
 *
 * @author ChecheBossxKun
 */
public class Recibo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       MetodosRecibo recibo = new MetodosRecibo();
       recibo.setNumContrato(102); 
       recibo.setFechaPago("21 Marzo 2019");
       recibo.setNombreEmpleado("Jose Lopez");
       recibo.setDomicilio("Av del Sol 1200");
       recibo.setTipoContrato(1);
       recibo.setNivelEstudios(1);
       recibo.setPagoBase(700.0f);
       recibo.setDiasTrabajados(15);   
       System.out.println("     El Calculo de el Subtotal es: " + recibo.calculoSubtotal());
       System.out.println("     El Calculo de el Impuesto es: " + recibo.calculoImpuesto());
       
       //OBJETO CONSTRUIDO POR ARGUMENTOS
       MetodosRecibo reci = new MetodosRecibo (102,"21 Marzo 2019","Jose Lopez","Av del Sol 1200",1,1,700.0f, 15);
       System.out.println ("Numero de Contrato:  " + reci.getNumContrato());
       System.out.println("Fecha de Pago:       " + reci.getFechaPago());
       System.out.println("Nombre Empleado:     " + reci.getNombreEmpleado());
       System.out.println("Domicilio:           " + reci.getDomicilio());
       System.out.println("Tipo de Contrato:    " + reci.getTipoContrato());
       System.out.println("Nivel de Estudios:   " + reci.getNivelEstudios());
       System.out.println("Pago Diario Base:    " + reci.getPagoBase());
       System.out.println("Dias Trabajados:     " + reci.getDiasTrabajados());
       
       //GENERA OBJETO CONSTRUIDO POR COPIA
       MetodosRecibo reci2 = new MetodosRecibo(recibo);
       System.out.println("     El SubTotal es: " + reci2.calculoSubtotal());
       System.out.println("     El Impuesto es: " + reci2.calculoImpuesto());
       System.out.println("     El Total a Pagar es:" + reci2.calculoTotal());
    }
}