/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package preexamen1;

/**
 *
 * @author ChecheBossxKun
 */
public class MetodosRecibo {
    private int numContrato;
    private String fechaPago;
    private String nombreEmpleado;
    private String domicilio;
    private int tipoContrato;
    private int nivelEstudios;
    private float pagoBase;
    private int diasTrabajados;
    private float calculoSubtotal;
   
    //CONSTRUCTORES
    public MetodosRecibo (){
        this.numContrato = 0;
        this.fechaPago = "";
        this.nombreEmpleado = "";
        this.domicilio = "";
        this.tipoContrato = 0;
        this.nivelEstudios = 0;
        this.pagoBase = 0.0f;
        this.diasTrabajados = 0;
    }
    
    public MetodosRecibo (int numContrato, String fechaPago, String nombreEmpleado, 
      String domicilio, int tipoContrato, int nivelEstudios, float pagoBase, int diasTrabajados){
        this.numContrato = numContrato;
        this.fechaPago = fechaPago;
        this.nombreEmpleado = nombreEmpleado;
        this.domicilio = domicilio;
        this.tipoContrato = tipoContrato;
        this.nivelEstudios = nivelEstudios;
        this.pagoBase = pagoBase;
        this.diasTrabajados = diasTrabajados;  
    }
    
    public MetodosRecibo (MetodosRecibo otro){
        this.numContrato = otro.numContrato;
        this.fechaPago = otro.fechaPago;
        this.nombreEmpleado = otro.nombreEmpleado;
        this.domicilio = otro.domicilio;
        this.tipoContrato = otro.tipoContrato;
        this.nivelEstudios = otro.nivelEstudios;
        this.pagoBase = otro.pagoBase;
        this.diasTrabajados = otro.diasTrabajados;
    }    

    //METODOS
    public int getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(int numContrato) {
        this.numContrato = numContrato;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public int getNivelEstudios() {
        return nivelEstudios;
    }

    public void setNivelEstudios(int nivelEstudios) {
        this.nivelEstudios = nivelEstudios;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }    
        
        //METODOS DE COMPORTAMIENTO
    
    public float calculoSubtotal (){
        float cSub = 0.0f;
        
        if (nivelEstudios == 1){
         cSub = this.pagoBase * 1.20f;      
        }
        
        else if (nivelEstudios == 2){
         cSub = this.pagoBase * 1.50f;
        }
        
        else if (nivelEstudios == 3){
         cSub = this.pagoBase * 2.0f;    
        }
        
        return cSub * this.diasTrabajados;
    }
    
    public float calculoImpuesto (){
        float cImpuesto = 0.0f;
        float impuesto = 0.16f;
        cImpuesto = (this.calculoSubtotal() * impuesto);
        return cImpuesto;
    }
    
    public float calculoTotal(){
        float cTotal = 0.0f;
        cTotal = (this.calculoSubtotal() - this.calculoImpuesto());
        return cTotal;
    }
}